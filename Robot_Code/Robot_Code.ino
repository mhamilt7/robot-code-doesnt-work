/*
   WHat does this do
 */
#include <SPI.h>
#include <RH_ASK.h>
//------------------------------------------------------------------------------
RH_ASK driver;
//int i;
//char rxbuffer[8];
//------------------------------------------------------------------------------
// motor one
int enA = 10;
int in1 = 9;
int in2 = 8;
// motor two
int enB = 5;
int in3 = 7;
int in4 = 6;
//------------------------------------------------------------------------------
//ultrasonic sensor
int trigPin = 3;
int echoPin = 4;
int ultrasonicDistance;
long duration;
//------------------------------------------------------------------------------
void setup()
{
    Serial.begin(9600);    //initialize serial, baudrate is 9600
    // enable ir receiver module
    if (!driver.init())
        Serial.println("init failed");

    //memset(rxbuffer,0,8);

    //motors
    pinMode(enA, OUTPUT);
    pinMode(enB, OUTPUT);
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);
    //US sensor
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
}
//------------------------------------------------------------------------------
void loop()
{
    //------------------------------------------------------------------------------
    ultrasonicDistance = getDistance();

    if (ultrasonicDistance < 10)
    {
        turnAround();
    }
    //------------------------------------------------------------------------------
    uint8_t buf[1];
    uint8_t buflen = 1;
//------------------------------------------------------------------------------
    if (driver.recv(buf, &buflen))
    {
        //    for (i = 0; i < buflen; i++)
        Serial.println(buf[0], HEX);
        switch (buf[0])
        {
        case 1:
            goStraight();
            break;
        case 2:
            goRight1();
            break;
        case 3:
            goRight2();
            break;
        case 4:
            goLeft1();
            break;
        case 5:
            goLeft2();
            break;
        case 0:
            Stop();
            break;
        }
        //------------------------------------------------------------------------------
//    Serial.print("Message: ");    //print "irCode: "
//    Serial.println(buf);
        delay(100); //delay 600ms
    }
}
