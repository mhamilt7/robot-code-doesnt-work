/*
  what is this
 */
#include <RH_ASK.h>
//sd card
#include <SPI.h>
#include <SD.h>
//sensor libraries
#include <Digital_Light_TSL2561.h>
#include <Wire.h>
//------------------------------------------------------------------------------
const bool debugMode = false;
unsigned long timeStamp;
//------------------------------------------------------------------------------
//integers
int greenPin = 9;
int yellowPin = 8;
int redPin = 5; //cant use 11
int inPin = 2;
int reading;
int trigPin = 6; //cant use 12
int echoPin = 7; //cant use 13
//------------------------------------------------------------------------------
int distance;
long duration;
int ultrasonicDistance;
//------------------------------------------------------------------------------
RH_ASK driver;
//------------------------------------------------------------------------------
void setup()
{
  Wire.begin();
  Serial.begin(9600);
  TSL2561.init();
  pinMode(greenPin, OUTPUT);
  pinMode(yellowPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(inPin, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  if (!driver.init())
    Serial.println("init filed");
}
//------------------------------------------------------------------------------
void loop()
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  ultrasonicDistance = duration * 0.034 / 2;


  reading = digitalRead(inPin);  //tilt sensor
  digitalWrite(yellowPin, (reading) ? LOW : HIGH);

  //light sensor
  digitalWrite(greenPin, (TSL2561.readVisibleLux() < 10) ? HIGH : LOW);

  digitalWrite(redPin, (ultrasonicDistance < 15) ? HIGH : LOW);

  rfTransmit(); //IR code transmitting


  printData();
  delay(50);
}
//------------------------------------------------------------------------------
void rfTransmit()
{
  uint8_t rfSendValue = 0xFF;
  if (reading != 0)
  {
    //    const char *Stop = "0xFF6897";
    //    driver.send((uint8_t *)Stop, strlen(Stop));
    rfSendValue = 0x00;
  } else if (ultrasonicDistance > 28 && ultrasonicDistance <= 32)
  {
    //    const char *Straight = "0xFF30CF";
    //    driver.send((uint8_t *)Straight, strlen(Straight));
    rfSendValue = 0x01;
    //right1
  } else if (ultrasonicDistance > 14 && ultrasonicDistance <= 28)
  {
    //    const char *Right1 = "0xFF18E7";
    //    driver.send((uint8_t *)Right1, strlen(Right1));
    rfSendValue = 0x02;

    //right2
  } else if (ultrasonicDistance > 1 && ultrasonicDistance <= 14)
  {
    //    const char *Right2 = "0xFF7A85";
    //    driver.send((uint8_t *)Right2, strlen(Right2));
    rfSendValue = 0x03;

    //left1
  } else if (ultrasonicDistance > 32 && ultrasonicDistance <= 46)
  {
    //    const char *Left1 = "0xFF10EF";
    //    driver.send((uint8_t *)Left1, strlen(Left1));
    rfSendValue =  0x04;

    //left2
  } else if (ultrasonicDistance > 46 && ultrasonicDistance <= 60)
  {
    //    const char *Left2 = "0xFF38C7";
    //    driver.send((uint8_t *)Left2, strlen(Left2));
    rfSendValue = 0x05;
  }
  driver.send(&rfSendValue, 1);
  driver.waitPacketSent();
  delay(50);

}
//------------------------------------------------------------------------------
void printData()
{
  Serial.println(digitalRead(inPin));
  Serial.print("The Light Value is: ");
  Serial.println(TSL2561.readVisibleLux());
  Serial.println(ultrasonicDistance);
}
